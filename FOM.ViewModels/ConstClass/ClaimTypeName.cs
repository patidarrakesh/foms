﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
   public class ClaimTypeName
    {
        public const string CLAIM_USERID = "CLAIM_USERID";
        public const string CLAIM_USERNAME = "CLAIM_USERNAME";
        public const string CLAIM_PERSON_INFORMATION = "CLAIM_PERSON_INFORMATION";
        public const string CLAIM_ROLE_TYPE = "CLAIM_ROLE_TYPE";
        public const string CLAIM_SELECTED_REGION_ID = "CLAIM_SELECTED_REGION_ID";
        public const string CLAIM_SELECTED_REGION_DETAILS = "CLAIM_REGION_DETAILS";
    }
}
