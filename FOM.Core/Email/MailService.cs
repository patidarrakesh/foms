﻿using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;

namespace FOM.Core
{
    public class MailService 
    {
        private static readonly Lazy<MailService> lazy = new Lazy<MailService>(() => new MailService());
        public static MailService Instance { get { return lazy.Value; } }
        private string emailFrom { get { return Convert.ToString(ConfigurationManager.AppSettings["emailFrom"]); } }
 
        private string userName { get { return Convert.ToString(ConfigurationManager.AppSettings["emailUserName"]); } }

        private string password { get { return Convert.ToString(ConfigurationManager.AppSettings["emailpassword"]); } }

        private string host { get { return Convert.ToString(ConfigurationManager.AppSettings["emailHost"]); } }

        private int port { get { return Convert.ToInt32(ConfigurationManager.AppSettings["emailPort"]); } }

        private bool UseDefaultCredentials { get { return Convert.ToBoolean(ConfigurationManager.AppSettings["emailUseDefaultCredentials"]); } }

        private int smtpClientTimeOut { get { return Convert.ToInt32(ConfigurationManager.AppSettings["emailsmtpClientTimeOut"]); } }
         

        /// <summary>
        /// Send Mail to emailTo via async 
        /// </summary>
        /// <param name="emailTo">Mail Id to sender user</param>
        /// <param name="mailbody">Mail Boby according to Templete</param>
        /// <param name="subject">Mail Subject according to Templete</param>
        /// <returns>if mail send its returm true othewise return false</returns>
        public async Task<bool> SendEmailAsync(string emailTo, string mailbody, string subject, Attachment attachment = null)
        {
            var from = new MailAddress(emailFrom);
            //var to = new MailAddress(emailTo);
            var enableSsl = false;//If use HTTPS in case use true otherwise use here false
            using (var mail = new MailMessage())
            {
                mail.From = from;
                foreach (var address in emailTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                //mail.Subject = SetMailValues(subject, keyValuePair);
                //mail.Body = SetMailValues(mailbody, keyValuePair);
                mail.Subject = subject;
                mail.Body = mailbody;

                mail.IsBodyHtml = true;

                //mail.ReplyToList.Add(new MailAddress(replyto, "My Email"));
                mail.ReplyToList.Add(from);
                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.Delay |
                                                   DeliveryNotificationOptions.OnFailure |
                                                   DeliveryNotificationOptions.OnSuccess;

                //Added to attach event calendar
                if (attachment != null)
                {
                    mail.Attachments.Add(attachment);
                }
                //Comment By Rakesh Mail For Stop Mail Sending 

                using (var client = new SmtpClient())
                {
                    client.Host = host;
                    client.EnableSsl = enableSsl;
                    client.Port = port;
                    client.UseDefaultCredentials = UseDefaultCredentials;
                    client.Timeout = smtpClientTimeOut;

                    if (!client.UseDefaultCredentials && !string.IsNullOrEmpty(userName) &&
                        !string.IsNullOrEmpty(password))
                    {
                        client.Credentials = new NetworkCredential(userName, password);
                    }

                    await client.SendMailAsync(mail);
                }
            }

            return true;
        }

        /// <summary>
        /// Create Key Pair value for Mail Subject and Body
        /// </summary>
        /// <param name="html">HTML value</param>
        /// <param name="keyValuePair">Replace key value in HTML</param>
        /// <returns>Formatted HTML Value</returns>
        public string SetMailValues(String html, List<KeyValuePair<string, string>> keyValuePair)
        {
            if (keyValuePair != null)
                for (int i = 0; i < keyValuePair.Count; i++)
                {
                    if (!string.IsNullOrWhiteSpace(keyValuePair[i].Key) && !string.IsNullOrWhiteSpace(keyValuePair[i].Value))
                        html = html.Replace("<<" + keyValuePair[i].Key.ToLower() + ">>", keyValuePair[i].Value);
                }
            return html;
        }

    }

    public static class AsyncHelpers
    {
        /// <summary>
        /// Execute's an async Task<T> method which has a void return value synchronously
        /// </summary>
        /// <param name="task">Task<T> method to execute</param>
        public static void RunSync(Func<Task> task)
        {
            var oldContext = SynchronizationContext.Current;
            var synch = new ExclusiveSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(synch);
            synch.Post(async _ =>
            {
                try
                {
                    await task();
                }
                catch (Exception e)
                {
                    synch.InnerException = e;
                }
                finally
                {
                    synch.EndMessageLoop();
                }
            }, null);
            synch.BeginMessageLoop();

            SynchronizationContext.SetSynchronizationContext(oldContext);
        }

        /// <summary>
        /// Execute's an async Task<T> method which has a T return type synchronously
        /// </summary>
        /// <typeparam name="T">Return Type</typeparam>
        /// <param name="task">Task<T> method to execute</param>
        /// <returns></returns>
        public static T RunSync<T>(Func<Task<T>> task)
        {
            var oldContext = SynchronizationContext.Current;
            var synch = new ExclusiveSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(synch);
            T ret = default(T);
            synch.Post(async _ =>
            {
                try
                {
                    ret = await task();
                }
                catch (Exception e)
                {
                    synch.InnerException = e;
                }
                finally
                {
                    synch.EndMessageLoop();
                }
            }, null);
            synch.BeginMessageLoop();
            SynchronizationContext.SetSynchronizationContext(oldContext);
            return ret;
        }

        private class ExclusiveSynchronizationContext : SynchronizationContext
        {
            private bool done;
            public Exception InnerException { get; set; }
            readonly AutoResetEvent workItemsWaiting = new AutoResetEvent(false);
            readonly Queue<Tuple<SendOrPostCallback, object>> items =
                new Queue<Tuple<SendOrPostCallback, object>>();

            public override void Send(SendOrPostCallback d, object state)
            {
                throw new NotSupportedException("We cannot send to our same thread");
            }

            public override void Post(SendOrPostCallback d, object state)
            {
                lock (items)
                {
                    items.Enqueue(Tuple.Create(d, state));
                }
                workItemsWaiting.Set();
            }

            public void EndMessageLoop()
            {
                Post(_ => done = true, null);
            }

            public void BeginMessageLoop()
            {
                while (!done)
                {
                    Tuple<SendOrPostCallback, object> task = null;
                    lock (items)
                    {
                        if (items.Count > 0)
                        {
                            task = items.Dequeue();
                        }
                    }
                    if (task != null)
                    {
                        task.Item1(task.Item2);
                        if (InnerException != null) // the method threw an exception
                        {
                            //throw new AggregateException("AsyncHelpers.Run method threw an exception.", InnerException);
                        }
                    }
                    else
                    {
                        workItemsWaiting.WaitOne();
                    }
                }
            }

            public override SynchronizationContext CreateCopy()
            {
                return this;
            }
        }
    }

}