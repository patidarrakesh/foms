﻿//custom js

function beginRequest() {
    $.blockUI({ message: '<div class="loader-overlay"><div class="loader">Loading...</div></div>' });
    //  resetCSS();
}
function beginRequestWithMessage(message) {
    $.blockUI({ message: '<div class="loader-overlay"><div class="loader">' + message + '</div></div>', baseZ: 2000 });
    //  resetCSS();
}

function endRequest() {
    $.unblockUI();
    // initializeCSS();
}

function openConfirmDialog(deletedId, titleText, okCallBackFunction, cancelCallBackFunction) {
    // e.preventDefault(); use this or return false
    bootbox.confirm(titleText, function (result) {
        if (result) {
            if (okCallBackFunction != null)
                okCallBackFunction(deletedId);
        }
        else {
            if (cancelCallBackFunction != null)
                cancelCallBackFunction();
        }
    });


    return false;
}
function openConfirmDeleteAttachmentDialog(docTypeId, deletedId, titleText, okCallBackFunction, cancelCallBackFunction) {
    // e.preventDefault(); use this or return false

    bootbox.confirm(titleText, function (result) {
        if (result) {
            if (okCallBackFunction != null)
                okCallBackFunction(docTypeId, deletedId);
        }
        else {
            if (cancelCallBackFunction != null)
                cancelCallBackFunction();
        }
    });


    return false;
}

function openConfirmSaveDialog(content, titleText, sender, yesCallBackFunction, noCallBackFunction) {
    bootbox.confirm({
        message: content,
        title: titleText,
        buttons: {
            
            confirm: {
                label: "Yes",
                className: "btn-primary margin-right-10",
                //callback: function () {
                //    if (noCallBackFunction != null)
                //        noCallBackFunction();
                //}
            },
            cancel: {
                label: "No",
                className: "btn-cancel pull-right",
                //callback: function () {
                //    if (yesCallBackFunction != null)
                //        yesCallBackFunction(sender);

                //}
            }
        },
        callback: function (result) {
            if(result)
            {
                if (yesCallBackFunction != null)
                    yesCallBackFunction(sender);
            }
            else
            {
                if (noCallBackFunction != null)
                    noCallBackFunction(sender);
            }
        }
    });
   // return false;
}


function showMessage(divID, className, message) {

    if (message != null && message != "") {
        // bootbox.alert(message);

        $("#" + divID).html(message);
        $("#" + divID).removeAttr('class').addClass("alert").addClass(className);
        //        $("#" + divID).slideDown();
        //        setTimeout(function () { $("#" + divID).slideUp(); setTimeout(function () { $("#" + divID).html("").removeAttr('class'); }, 2000); }, 5000);
        $(window).scrollTop(0);
    }
}

//show MessagePopup
function showMessagePopupWithCallBack(divID, className, message, callBackFunction) {

    var header = '';
    if (className != null && className != '') {
        header = className.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
    }
    if (message != null && message != "") {
        // bootbox.alert(message);


        //Set Message
        $("#" + divID + "-text").html(message);
        //set Message Type
        $("#" + divID + "-container").removeAttr('class').addClass("modal-content").addClass(className);

        //Set Header
        $("#" + divID + " h1").html(header);
        //Show Popup
        $("#" + divID).modal('toggle');
        if (callBackFunction != undefined) {
            callBackFunction();
        }
    }
}



function openDialog(url, titleText, dialogID, removeClasses) {

    beginRequest();
    $.ajax({
        type: "GET",
        url: encodeURI(url),
        cache: false,
        dataType: 'html',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // Security Issue Point 12, 12Jan2015
            if (XMLHttpRequest.status != 403) {
                $("#modal-body-content").html(XMLHttpRequest.responseText);
            }
        },
        success: function (data, textStatus, XMLHttpRequest) {

            $("#modal-body-content").html(data);
        },
        complete: function (XMLHttpRequest, textStatus) {
            $("#modal-body-title").addClass("modelbox-title").html(titleText);
            //if (removeClasses == null) {
            //$('#lightbox').addClass("model-full");
            //    $('#mapModal').removeClass("model-medium");
            //}
            $('#lightbox').modal('toggle');
            endRequest();
        }
    });

}

/*Alert Message*/
function showMessagePopup(divID, className, message) {

    var header = '';
    if (className != null && className != '') {
        header = className.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
    }
    if (message != null && message != "") {
        // bootbox.alert(message);


        //Set Message
        $("#" + divID + "-text").html(message);
        //set Message Type
        $("#" + divID + "-container").removeAttr('class').addClass("modal-content").addClass(className);


        //Set Header
        $("#" + divID + " h4").html(header);
        //Show Popup
        $("#" + divID).modal('toggle');

        setTimeout(function () {

            if ($('#divMessage-container').css('display') == 'block' && className.toLowerCase() == 'success') {
                $('#divMessage-container .close').click()

            }
        }, 5000);
    }
}


function showDialog(divID,heading,content)
{
    if (content != null && content != "") {
        // bootbox.alert(message);

        //
        $("#" + divID + "-text").html('');
        //Set Message
        $("#" + divID + "-text").html(content);
        //set Message Type
        $("#" + divID + "-container").removeAttr('class').addClass("modal-content").addClass("success");


        //Set Header
        $("#" + divID + " #popup_heading").html(heading);
        //Show Popup
        $("#" + divID).modal('toggle');
    }
}


function UploadFileValidation(ctrl, ExculdeFiles, AllowedMaxFileSize) {
    var excludeFileExtension = ExculdeFiles.split(",");// ['exe', 'dll'];
  
    if (ctrl.files != null) {
        var control = $(ctrl);
        var fileName = $(ctrl).val().replace(/^.*[\\\/]/, '');
        var fileId = $(ctrl).attr("id");
        var sizeInMB = 0;
        if (ctrl.files.length > 0) {
            sizeInMB = ctrl.files[0].size / 1048576;

            var ext = $(ctrl).val().split('.').pop().toLowerCase();
            if ($.inArray(ext, excludeFileExtension) != -1) {

                bootbox.alert("Invalid File Extension", function () {
                    $(ctrl).val("");
                    $("#uniform-" + fileId + " .filename").text("No file selected");
                    control.replaceWith(control = control.clone(true));
                    
                    control.uniform();
                });
            }

            else if (sizeInMB > parseFloat(AllowedMaxFileSize) / 1048576) {

                var message = '';
                if (parseInt(AllowedMaxFileSize) / 1048576 > 1)
                    message = parseInt(AllowedMaxFileSize) / 1048576 + " MB";
                else if (parseInt(AllowedMaxFileSize) / 1048576 < 1 && parseInt(AllowedMaxFileSize) / 1024 > 1)
                    message = parseInt(AllowedMaxFileSize) / 1024 + " KB";
                else message = parseInt(AllowedMaxFileSize) + " Bytes";
                bootbox.alert("File size should be less than " + message, function () {
                    $(ctrl).val("");
                    $("#uniform-" + fileId + " .filename").text("No file selected");
                    control.replaceWith(control = control.clone(true));
                    control.uniform();
                });
            }
            else {
                $("#uniform-" + fileId + " .filename").text(fileName);
            }
        }        
        $("#" + fileId).valid();
    }
}