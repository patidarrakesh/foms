﻿

function checkboxAllSelected(allId, singleId, selectedId) {
    if ($("#" + allId).is(":checked") == true) {
        $("input[id ^='" + singleId + "' ]").prop("checked", true);
        $("input[id ^='" + singleId + "' ]").each(function () {
            var id = this.id.split("_");
            addChecked(id[1], selectedId);
        })
    } else {
        $("input[id ^='" + singleId + "' ]").prop("checked", false);
        $("input[id ^='" + singleId + "' ]").each(function () {
            var id = this.id.split("_");
            removeChecked(id[1], selectedId);
        })
    }
    //$("input:checkbox").uniform();
}

function checkboxSingleSelected(singleId, allId, selectedId) {
    var id = singleId.split("_");
    if ($("#" + singleId).is(":checked") == true) { addChecked(id[1], selectedId); }
    else { removeChecked(id[1], selectedId); }
    if ($("input[id ^='" + id[0] + "_' ]:checked").length == $("input[id ^='" + id[0] + "_' ]").length) {
        $("#" + allId).prop("checked", true);
    } else {
        $("#" + allId).prop("checked", false);
    }
    //$("input:checkbox").uniform();
}

function addChecked(id, selectedId) {
    var selectedItems = $("#" + selectedId).val();
    if (typeof (selectedItems) != "undefined" && selectedItems != "") {
        var itemArray = selectedItems.split(",");
        if ($.inArray(id, itemArray) == -1) {
            itemArray.push(id);
        }
        $("#" + selectedId).val(itemArray.join(","));
    }
    else {
        $("#" + selectedId).val(id);
    }
}

function removeChecked(id, selectedId) {
    var selectedItems = $("#" + selectedId).val();
    if (typeof (selectedItems) != "undefined" && selectedItems != "") {
        var itemArray = selectedItems.split(",");
        if ($.inArray(id, itemArray) != -1) {
            itemArray = jQuery.grep(itemArray, function (value) {
                return value != id;
            });
        }
        $("#" + selectedId).val(itemArray.join(","));
    }
}


function checkOld(selectedId, allId, singleId) {
    var selectedItems = $("#" + selectedId).val();
    if (typeof (selectedItems) != "undefined" && selectedItems != "") {
        var itemArray = selectedItems.split(",");
        for (var i = 0; i < itemArray.length; i++) {
            $("#" + singleId + itemArray[i]).prop("checked", true);
        }
        allChecked(singleId, allId);
    }
    else {
        $("input[id ^='" + singleId + "' ]").prop("checked", false);
    }
    //$("input:checkbox").uniform();
}

function allChecked(singleId, allId) {
    if ($("input[id ^='" + singleId + "' ]:checked").length == $("input[id ^='" + singleId + "' ]").length) {
        $("#" + allId).prop("checked", true);
    } else {
        $("#" + allId).prop("checked", false);
    }
}
