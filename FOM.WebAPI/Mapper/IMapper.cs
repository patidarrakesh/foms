﻿using JKApi.Business.Domain;
using FOM.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FOM.WebAPI.Mapper
{
    public interface IMapper
    {
        IDomainBase ModelToDomain(IModelBase modelObj);

        IModelBase DomainToModel(IDomainBase domainObj);
    }
}