﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JKApi.Business.Domain;
using FOM.WebAPI.Models;
using FOM.WebAPI.Models.Account;

namespace FOM.WebAPI.Mapper
{
    public class AccountMapper : IMapper
    {
        public IModelBase DomainToModel(IDomainBase domainObj)
        {
            if (domainObj == null)
                return null;
            if (!(domainObj is User))
                throw new Exception(); //TODO: Update this with better handling
            User user = domainObj as User;
            LoginResponseModel loginResponseModel = new LoginResponseModel();
            loginResponseModel.Id = user.ID.Value;
            loginResponseModel.Username = user.Username;
            loginResponseModel.Key = user.Password;
            return loginResponseModel;
        }

        public IDomainBase ModelToDomain(IModelBase modelObj)
        {
            if (modelObj == null)
                return null;
            if (!(modelObj is LoginResponseModel))
                throw new Exception(); //TODO: Update this for better error handling
            LoginResponseModel loginResponseModel = modelObj as LoginResponseModel;
            User user = new User();
            user.Username = loginResponseModel.Username;
            user.Password = loginResponseModel.Key;
            user.ID = loginResponseModel.Id;
            return user;
        }
    }
}