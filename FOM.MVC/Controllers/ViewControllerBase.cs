﻿using FOM.Core;
using FOM.Service;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FOM.MVC
{
    public class ViewControllerBase : Controller
    {
        //These are common for all
        protected EncryptDecrypt _encryptDecrypt = EncryptDecrypt.Instance;
        protected MailService _mailService = MailService.Instance;
        protected ClaimView _claimView = ClaimView.Instance;
        protected NLogger _nLogger = NLogger.Instance;
        protected CacheProvider _cacheProvider = CacheProvider.Instance;
        
        protected IJobService _jobService;
        protected IClientService _clientService;
        protected IDashboardService _dashboardService;
        protected IEmployeeService _employeeService;
        protected IScheduleService _scheduleService;
        protected IReportService _reportService;
        protected IMapService _mapService;
        protected IOtherService _otherService;
        protected IUserService _userService;
        #region Constructor

        public ViewControllerBase()
        {

        }

        protected string CurrentUserIdentityName
        {
            get
            {
                return System.Web.HttpContext.Current == null
                           ? String.Empty
                           : System.Web.HttpContext.Current.User.Identity.Name;
            }
        }

        protected string CurrentUserIdentityShortName
        {
            get
            {
                return System.Web.HttpContext.Current == null
                            ? String.Empty
                            : GetShortUserName(CurrentUserIdentityName);
            }
        }

        #endregion

        /// <summary>
        /// In controllers that catch their own exceptions, call this to send to the error view
        /// </summary>
        /// <param name="errorMsg">A <see cref="String"/> representing the error message</param>
        /// <returns><see cref="System.Web.Mvc.RedirectToRouteResult"/> routes to an error view</returns>
        protected PartialViewResult HandleErrorConditionPartial(string errorMsg)
        {
            return PartialView("~/Views/Error/ExceptionError.cshtml", errorMsg);
        }

        /// <summary>
        /// In controllers that catch their own exceptions, call this to send to the error view
        /// </summary>
        /// <param name="errorMsg">A <see cref="String"/> representing the error message</param>
        /// <returns><see cref="System.Web.Mvc.RedirectToRouteResult"/> routes to an error view</returns>
        protected ActionResult HandleErrorCondition(string errorMsg)
        {
            return RedirectToAction("ExceptionError", "Error", new { @area = "", msg = errorMsg });
        }

         

        // TODO : Refactor.  Why is there an indexer being created on Controllers that tries to return a session object?
        public object this[string index]
        {
            get
            {
                return HttpContext.Session[index];
            }
            set
            {
                HttpContext.Session[index] = value;
            }
        }


        public long PrimaryKey
        {
            get
            {
                return ViewBag.PrimaryKey;
            }
            set
            {
                ViewBag.PrimaryKey = value;
            }
        }

        

        public string GetShortUserName(string longName)
        {
            int stop = longName.IndexOf("\\", System.StringComparison.Ordinal);
            return (stop > -1) ? longName.Substring(stop + 1, longName.Length - stop - 1) : longName;
        } 
    }
}
