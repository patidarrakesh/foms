﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FOM.MVC.Startup))]
namespace FOM.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
