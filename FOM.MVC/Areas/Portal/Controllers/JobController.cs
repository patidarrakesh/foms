﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;

namespace FOM.MVC.Areas.Portal.Controllers
{
    public class JobController : ViewControllerBase
    {

        public JobController()
        {
            ViewBag.HMenu = VMenuEnum.Job.ToString();
        }

        // GET: Portal/Job
        public ActionResult Index()
        {
            BreadCrumb.Add(Url.Action("Index", "Job", new { area = "Portal" }), "Job");
            return View();
        }
    }
}