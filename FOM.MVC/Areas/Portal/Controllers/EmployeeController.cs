﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;


namespace FOM.MVC.Areas.Portal.Controllers
{
    public class EmployeeController : ViewControllerBase
    {

        public EmployeeController()
        {
            ViewBag.HMenu = VMenuEnum.Employee.ToString();
        }

        // GET: Portal/Employee
        public ActionResult Index()
        {
            BreadCrumb.Add(Url.Action("Index", "Employee", new { area = "Portal" }), "Employee");
            return View();
        }
    }
}