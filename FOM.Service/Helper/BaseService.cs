﻿
using System;
using System.Data;
using System.Reflection;

namespace FOM.Service
{
    public class BaseService
    {
         
        public T GetDataRowToEntity<T>(DataRow dr) where T : new()
        {

            string propName = string.Empty;

            try
            {
                var objT = new T();
                PropertyInfo[] objprop = objT.GetType().GetProperties();
                foreach (PropertyInfo pr in objprop)
                {
                    if (dr.Table.Columns[pr.Name] != null && dr[pr.Name] != null && dr[pr.Name] != DBNull.Value)
                    {
                        propName = pr.Name;
                        var obj = dr[pr.Name];
                        pr.SetValue(objT, obj, null);
                    }
                }
                return objT;
            }
            catch (Exception x)
            {
                throw;
            }

        }
    }
}
