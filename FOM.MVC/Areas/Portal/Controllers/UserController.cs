﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using FOM.Service;
using FOM.Core;
using FOM.ViewModels;
using FOM.Resources;

namespace FOM.MVC.Areas.Portal.Controllers
{
    public class UserController : ViewControllerBase
    {
        // GET: JKControl/User
        public UserController(IUserService userService)
        {
            this._userService = userService;
        }

        #region Login/LogOFF
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            UserLoginViewModel userLoginViewModel = new UserLoginViewModel();
            ViewBag.ReturnUrl = returnUrl;
            if (Request.Cookies["RememberMe"] != null)
            {
                userLoginViewModel.Username = _encryptDecrypt.Decrypt(Request.Cookies["RememberMe"].Values["UserName"].ToString());
                userLoginViewModel.RememberMe = true;
            }

            return View(userLoginViewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Login(UserLoginViewModel inputUserModel, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                inputUserModel.ErrorMessage = CommonResource.msgModelStateError;
                return View(inputUserModel);
            }

            UserViewModel userViewModel = _userService.Login(inputUserModel);

            //Proceed if no errors            
            if (userViewModel != null && userViewModel.objErrorModel != null && userViewModel.objErrorModel.Count == 0)
            {
                //If the user is already authenticated, then remove the claims
                bool isAlreadyAuthenticated = false;
                ClaimsIdentity newIdentityFromExisting = System.Threading.Thread.CurrentPrincipal.Identity as ClaimsIdentity;

                //if (HttpContext.Current.User != null && HttpContext.Current.User.Identity != null && HttpContext.Current.User.Identity.IsAuthenticated)
                if (newIdentityFromExisting != null && newIdentityFromExisting.Claims != null && newIdentityFromExisting.Claims.Count() > 1) //If existing identity exists and has multiple claims
                {
                    //Try removing all the claims from the existing identity
                    Claim[] claimsArray = newIdentityFromExisting.Claims.ToArray();
                    for (int index = 0; index < claimsArray.Count(); index++)
                    {
                        Claim claim = claimsArray[index];
                        newIdentityFromExisting.TryRemoveClaim(claim);
                    }

                    //set isAlreadyAuthenticated as 'true'
                    isAlreadyAuthenticated = true;
                }

                //Prepare the user claims identity and add user details
                ClaimsIdentity userClaimsIdentity = !isAlreadyAuthenticated ? new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie) : newIdentityFromExisting;
                userClaimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userViewModel.UserId.ToString()));
                userClaimsIdentity.AddClaim(new Claim(ClaimTypeName.CLAIM_USERNAME, userViewModel.UserName));

                //Added person model into claim. 
                userClaimsIdentity.AddClaim(new Claim(ClaimTypeName.CLAIM_PERSON_INFORMATION, JsonConvert.SerializeObject(userViewModel)));
                userClaimsIdentity.AddClaim(new Claim(ClaimTypeName.CLAIM_USERID, Convert.ToString(userViewModel.UserId)));

                if (userViewModel.Regions != null && userViewModel.Regions.Count > 0)
                {
                    userClaimsIdentity.AddClaim(new Claim(ClaimTypeName.CLAIM_SELECTED_REGION_ID, Convert.ToString(userViewModel.Regions.FirstOrDefault().RegionId)));
                    userClaimsIdentity.AddClaim(new Claim(ClaimTypeName.CLAIM_SELECTED_REGION_DETAILS, JsonConvert.SerializeObject(userViewModel.Regions.FirstOrDefault())));
                }

                if (userViewModel.Roles != null && userViewModel.Roles.Count > 0)
                {
                    //Claims for the user roles
                    foreach (var role in userViewModel.Roles)
                    {
                        userClaimsIdentity.AddClaim(new Claim(ClaimTypes.Role, role.RoleName));

                        if (!userClaimsIdentity.Claims.Any(clm => clm.Type == ClaimTypeName.CLAIM_ROLE_TYPE && clm.Value == role.RoleType))
                            userClaimsIdentity.AddClaim(new Claim(ClaimTypeName.CLAIM_ROLE_TYPE, role.RoleType));
                    }
                }

                //If not already authenticated, then generate a new ticket
                if (!isAlreadyAuthenticated)
                {
                    AuthenticationProperties authProperties = new AuthenticationProperties();
                    authProperties.IsPersistent = inputUserModel.RememberMe;
                    HttpContext.GetOwinContext().Authentication.SignIn(authProperties, userClaimsIdentity);
                }
                //Else update the existing ticket
                else
                {
                    //Regenerate user identity
                    HttpContext.GetOwinContext().Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(userClaimsIdentity), new AuthenticationProperties() { IsPersistent = inputUserModel.RememberMe });
                }

                //Set the user's identity in the current thread for other requests to API on the same thread                
                System.Threading.Thread.CurrentPrincipal =
                   new GenericPrincipal(
                       userClaimsIdentity,
                       userViewModel.Roles.Select(role => role.RoleName).ToArray()
                   );


                if (inputUserModel.RememberMe)
                {
                    HttpCookie cookie = new HttpCookie("RememberMe");
                    cookie.Values.Add("UserName", _encryptDecrypt.Encrypt(inputUserModel.Username));
                    cookie.Expires = DateTime.Now.AddDays(Convert.ToInt32(WebConfigResource.RememberMeExpireIn_Days));
                    Response.Cookies.Add(cookie);
                }
                else
                {
                    Response.Cookies["RememberMe"].Expires = DateTime.Now.AddDays(-1);
                }

                return RedirectToAction("Index", "JKHome", new { area = "JKControl" });
            }
            else
            {
                inputUserModel.ErrorMessage = userViewModel.objErrorModel[0].ErrorMessage;
            }
            return View(inputUserModel);
        }

        public ActionResult LogOff()
        {
            Session.RemoveAll();
            Session.Abandon();

            //Sign out user from identity
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Login", "User", new { area = "JKControl" });
        }
        #endregion


    }
}