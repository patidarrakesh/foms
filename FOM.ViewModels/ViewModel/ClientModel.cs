﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOM.ViewModels
{
    public class ClientModel : BaseModel
    {
      
        public ClientModel()
        {
            Address = new AddressModel();
        }

        public int ClientId { get; set; }
        public string ClientCode { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public int? AddressId { get; set; }
        
        public AddressModel Address { get; set; }
        
    }

    public class ViewClientListModel : PaggingModel
    {
        public ViewClientListModel()
        {
            clientList = new List<ClientModel>();
        }

        public string SearchText { get; set; }

        public int ActiveStatus { get; set; }

        public List<ClientModel> clientList { get; set; }
    }
}
