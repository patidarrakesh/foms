﻿using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FOM.ViewModels;
using FOM.Service;
using FOM.Resources;

namespace FOM.MVC.Areas.Portal.Controllers
{
    public class ClientController : ViewControllerBase
    {

        public ClientController(IClientService clientService)
        {
            this._clientService = clientService;
            ViewBag.HMenu = VMenuEnum.Client.ToString();
        }

        // GET: Portal/Client
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Save()
        {
            return View();
        }

        #region Client Listing

        //[MenuAccess]
        [HttpGet]
        public ActionResult ClientListing()
        {
            ViewClientListModel viewClientListModel = new ViewClientListModel()
            {
                PageSize = 2,// Convert.ToInt32(WebConfigResource.ListPageSize),
                RegionId = 1// _claimView.GetCLAIM_SELECTED_REGION_ID()
            };

            viewClientListModel = _clientService.ClientList(viewClientListModel);
            return View(viewClientListModel);
        }

        [HttpPost]
        public ActionResult ClientListing(ViewClientListModel viewClientListModel)
        {
            viewClientListModel = _clientService.ClientList(viewClientListModel);
            return PartialView("_clientList", viewClientListModel);
        }


        public ActionResult DeactivateClient(int Row)
        {
            ViewClientListModel viewClientListModel = new ViewClientListModel();
            return Json(new { Message = viewClientListModel.Message, MessageType = viewClientListModel.MessageType }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ActivateClient(int Row)
        {
            ViewClientListModel viewClientListModel = new ViewClientListModel();
            return Json(new { Message = viewClientListModel.Message, MessageType = viewClientListModel.MessageType }, JsonRequestBehavior.AllowGet);
        }


        #endregion
    }
}