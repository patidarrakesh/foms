using FOM.Core;
using FOM.Service;
using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;

namespace FOM.WebAPI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

             

            container.RegisterType<IJobService, JobService>();
            container.RegisterType<IScheduleService, ScheduleService>();
            container.RegisterType<IClientService, ClientService>();
            container.RegisterType<IEmployeeService, EmployeeService>();
            container.RegisterType<IMapService, MapService>();
            container.RegisterType<IReportService, ReportService>();
            container.RegisterType<IOtherService, OtherService>();
            container.RegisterType<IDashboardService, DashboardService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}