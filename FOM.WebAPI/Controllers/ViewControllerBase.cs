﻿using FOM.Core;
using FOM.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FOM.WebAPI.Controllers
{
    public class ViewControllerBase : ApiController
    {
        //These are common for all
        protected EncryptDecrypt _encryptDecrypt = EncryptDecrypt.Instance;
        protected MailService _mailService = MailService.Instance;
        protected ClaimView _claimView = ClaimView.Instance;
        protected NLogger _nLogger = NLogger.Instance;
        protected CacheProvider _cacheProvider = CacheProvider.Instance;

        protected IJobService _jobService;
        protected IClientService _clientService;
        protected IDashboardService _dashboardService;
        protected IEmployeeService _employeeService;
        protected IScheduleService _scheduleService;
        protected IReportService _reportService;
        protected IMapService _mapService;
        protected IOtherService _otherService;
        protected IUserService _userService;

    }
}
